import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { delay, finalize } from 'rxjs/operators';
import {ThemePalette} from '@angular/material/core';
import {ProgressSpinnerMode} from '@angular/material/progress-spinner';
 
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  predection:String;
  isLoading:boolean;
  link:string;


  movieIdFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  color: ThemePalette = 'warn';
  mode: ProgressSpinnerMode = 'indeterminate';
  value = 50;
  constructor(private http:HttpClient) {
    this.predection=null;
   
    this.link=null;
    
   }

  topTrending()
  {
    console.log('topTrending')
    this.predection=null;
    this.isLoading=true;
    this.http.post<any>('http://165.22.233.161:5000/classify',{url:this.link}).pipe(delay(3000)).
    pipe(finalize(()=>this.isLoading=false)).
     subscribe(data=>{this.predection=data.data;console.log(data)})
  }

 

  ngOnInit(): void {
   
  }

}
